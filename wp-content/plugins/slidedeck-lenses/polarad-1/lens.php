<?php
class SlideDeckLens_Polarad1 extends SlideDeckLens_Scaffold {
    var $three_wide_top_offset = 10;
    var $options_model = array(
        'Appearance' => array(
            'accentColor' => array(
                'value' => "#300"
            ),
            'hideSpines' => array(
                'type' => 'hidden',
                'value' => true
            ),
            'titleFont' => array(
                'value' => 'marker'
            ),
            'bodyFont' => array(
                'value' => 'marker'
            ),
            'layout_style' => array(
                'type' => 'select',
                'data' => 'string',
                'value' => "_blank",
                'values' => array(
                    'layout-single' => "Single",
                    'layout-three-wide' => "Three Wide (height determined by width)",
                    'layout-peek' => "Peek (shows the edges of neighboring slides)",
                    'layout-stacked' => "Stacked (slightly rotated stack)",
                ),
                'label' => "Layout Style",
                'weight' => 30
            ),
            'show-drop-shadow' => array(
                'name' => 'show-drop-shadow',
                'type' => 'radio',
                'data' => 'boolean',
                'label' => "Show drop shadow?",
                'value' => true,
                'weight' => 40
            ),
            'show-tape' => array(
                'name' => 'show-tape',
                'type' => 'radio',
                'data' => 'boolean',
                'label' => "Show tape?",
                'value' => true,
                'weight' => 50
            ),
        ),
        'Navigation' => array(
            'arrow-style' => array(
                'name' => 'arrow-style',
                'type' => 'select',
                'values' => array(
                    'arrowstyle-1' => 'Default',
                    'arrowstyle-2' => 'Cursive',
                    'arrowstyle-3' => 'Tape',
                    'arrowstyle-4' => 'Sketch',
                ),
                'value' => 'arrowstyle-1',
                'label' => 'Arrow Style',
                'description' => "Pick an arrow style that suits you.",
                'weight' => 40,
                'interface' => array(
                    'type' => 'thumbnails-flyout',
                    'values' => array() // deinfed in construct
                )
            ),
        ),
        'Content' => array(
            'show-likes' => array(
                'name' => 'show-likes',
                'type' => 'radio',
                'data' => 'boolean',
                'label' => "Show like count?",
                'value' => true,
                'description' => "Some sources provide likes via the API (dribbble, 500px, Instagram) should we show them?"
            ),
            'date-format' => array(
                'type' => 'hidden'
            ),
            'show-excerpt' => array(
                'type' => 'hidden',
                'value' => false
            ),
            'show-readmore' => array(
                'type' => 'hidden',
                'value' => false
            ),
            'excerptLengthWithImages' => array(
                'type' => 'hidden',
                'value' => false
            ),
            'excerptLengthWithoutImages' => array(
                'type' => 'hidden',
                'value' => false
            ),
            'titleLengthWithoutImages' => array(
                'type' => 'hidden',
                'value' => false
            ),
        ),
        'Behavior' => array(
            'slideTransition' => array(),
            'transition' => array(
                'value' => 'easeOutExpo'
            )
        )
    );
    
    var $slide_size = false;
    
    /**
     * The SlideSize Percent is largely arbitrary, but is needed
     * to make the three-wide style look good and show full titles at 
     * different sizes.
     */
    var $three_wide_slide_size_percent = 0.45;
    
    function __construct(){
        parent::__construct();
        
        $lens_url = plugins_url() . '/slidedeck-lenses/polarad/images/';
        $this->options_model['Navigation']['arrow-style']['interface']['values'] = array(
            'arrowstyle-1' => $lens_url . 'arrowstyle_1.thumb.png',
            'arrowstyle-2' => $lens_url . 'arrowstyle_2.thumb.png',
            'arrowstyle-3' => $lens_url . 'arrowstyle_3.thumb.png',
            'arrowstyle-4' => $lens_url . 'arrowstyle_4.thumb.png',
        );
        
        add_filter( "{$this->namespace}_get_slides", array( &$this, "slidedeck_get_slides" ), 11, 2 );
        add_filter( "{$this->namespace}_shortcode_atts", array( &$this, "modify_shortcode_atts" ) );
        add_filter( "{$this->namespace}_lazy_load_padding", array( &$this, "modify_lazy_load_padding" ), 10, 2 );
    }
    
    /**
     * Hook into modify some options
     * 
     * @param array $options_model Options Model array
     * @param arrat $slidedeck The SlideDeck object
     * 
     * @uses SlideDeckLens_Scaffold::is_valid()
     * 
     * @return array
     */
    function slidedeck_options_model( $options_model, $slidedeck ){
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            if( isset( $slidedeck['options']['layout_style'] ) && !empty( $slidedeck['options']['layout_style'] ) ){
                if( $slidedeck['options']['layout_style'] == 'layout_stacked' ){
                    // Set the transtion to stack
                    $options_model['Transitions']['slideTransition'] = array_merge( 
                        $options_model['Transitions']['slideTransition'],
                        array(
                            'value' => 'stack'
                        )
                    );
                }
            }
        }
        return $options_model;
    }
    
    function slidedeck_render_slidedeck_after($html, $slidedeck){
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            $html .= '<div class="sd2-shadow-element"></div>';
            $html .= '<div class="sd2-tape-element"></div>';
        }
        return $html;
    }
    
    /**
     * Hooks into the shortcode attributes so we can 
     * control the proportional option for this particualrly
     * esoteric lens.
     */
    function modify_shortcode_atts($atts){
        global $SlideDeckPlugin;
        
        // Get the meta that says if we are using this lens.
        $lens = get_post_meta( $atts['id'], "{$this->namespace}_lens", true );
        
        // As long as we are indeed using this lens, we can go on...
        if( $this->is_valid( $lens ) ) {
            // Get the deck itself. We need to see the options.
            $slidedeck = $SlideDeckPlugin->SlideDeck->get( $atts['id'] );
            /**
             * If the layout style is either stacked or three-wide, then
             * we need to force proportional. Proportional RESS messes with
             * these, so it needs to be disallowed.
             */
            switch( $slidedeck['options']['layout_style'] ) {
                case 'layout-stacked':
                case 'layout-three-wide':
                    $atts['proportional'] = true;
                break;
            }
        }
        return $atts;
    }
    
    /**
     * TODO:
     * Let's conssider refactoring this in the future.
     * The point of this function is to get the closest size for a deck
     * where the <dd> elements are a much different size than the <dl>
     */
    /**
     * Get the closest SlideDeck class for custom dimensions
     * 
     * This is a duplication of the core method SlideDeck::get_closest_size()
     * 
     * @param object $slidedeck A SlideDeck
     * @param float $percent A float value 0.0 - 1.0 representing the "scale factor"
     * 
     * @uses apply_filters()
     * 
     * @return string
     */
    function get_closest_size_percent( $slidedeck, $percent = 1.0 ) {
        global $SlideDeckPlugin;
        
        $width = $slidedeck['options']['width'] * $percent;
        $height = $slidedeck['options']['height'] * $percent;
        
        $sizes = apply_filters( "slidedeck_sizes", $SlideDeckPlugin->sizes, $slidedeck );
        $previous_width_delta = 99999;
        foreach( $sizes as $size => $properties ) {
            // Determine the delta between this "size" and the user specified width
            $width_delta = abs( $properties['width'] - $width );
            // The closest delta gets the size class
            if( $width_delta < $previous_width_delta ) {
                $previous_width_delta = $width_delta;
                $closest_size = $size;
            } 
        }
        return $closest_size;
    }

    function modify_lazy_load_padding( $padding, $slidedeck ) {
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            switch( $slidedeck['options']['layout_style'] ){
                case 'layout-three-wide':
                case 'layout-peek':
                    $padding = $padding + 4;
                break;
            }
        }
        return $padding;
    }
    
    
    function slidedeck_dimensions($width, $height, $outer_width, $outer_height, $slidedeck){
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            switch( $slidedeck['options']['layout_style'] ){
                case 'layout-single':
                    // Extra space
                    $title_height = 40;
                    if( ( $slidedeck['options']['size'] == 'small' ) || ( ( $this->slide_size == 'small' ) ) ){
                        $title_height = 15;
                    }
                    $width = $width - $title_height;
                break;
                case 'layout-stacked':
                    // Extra space
                    $title_height = 40;
                    if( ( $slidedeck['options']['size'] == 'small' ) || ( ( $this->slide_size == 'small' ) ) ){
                        $title_height = 15;
                    }
                    $height = $outer_height = $width;
                    $width = $width - $title_height;
                break;
                case 'layout-three-wide':
                    $this->slide_size = $this->get_closest_size_percent( $slidedeck, $this->three_wide_slide_size_percent );
                    
                    $width_factor = 40;
                    If( $this->slide_size == 'small' ) {
                        $width_factor = 20;
                    }
                    $width = round( $outer_width * 0.33 );
                    $height = round( ( $outer_width * 0.33 ) + $width_factor );
                    
                    $outer_height = $outer_width * 0.33 + $width_factor + $this->three_wide_top_offset;
                    
                    // Extra space
                    $title_height = 40;
                    if( ( $slidedeck['options']['size'] == 'small' ) || ( ( $this->slide_size == 'small' ) ) ){
                        $title_height = 0;
                    }
                break;
                case 'layout-peek':
                    $factor = 0.68;
                    if( $width >= $height ){
                        // Wide
                        $height = round( $outer_height * $factor );
                        $width = $height;
                    }else{
                        // Tall
                        $width = round( $outer_width * $factor );
                        $height = $width;
                    }
                    // Extra space
                    $title_height = 40;
                    if( ( $slidedeck['options']['size'] == 'small' ) || ( ( $this->slide_size == 'small' ) ) ){
                        $title_height = 15;
                    }
                    $width = $width - $title_height;
                break;
            }

        }
    }
    
    /**
     * Filter the styles that are on the deck
     * 
     * @param array $styles an array of styles and values
     * @param array $slidedeck The slidedeck object 
     * 
     * @return array 
     */
    function slidedeck_styles_arr( $styles, $slidedeck ) {
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            $styles['margin-top'] = '-' . round( intval( $styles['height'] ) / 2 ) - round( $this->three_wide_top_offset / 2 ) . 'px';
        }
        return $styles;
    }
    
    /**
     * Add appropriate classes for this Lens to the SlideDeck frame
     * 
     * @param array $slidedeck_classes Classes to be applied
     * @param array $slidedeck The SlideDeck object being rendered
     * 
     * @return array
     */
    function slidedeck_frame_classes( $slidedeck_classes, $slidedeck ) {
        if( $this->is_valid( $slidedeck['lens'] ) ) {
            if( isset( $slidedeck['options']['layout_style'] ) && !empty( $slidedeck['options']['layout_style'] ) ){
                $slidedeck_classes[] = $slidedeck['options']['layout_style'];
            }
            
            $slidedeck_classes[] = $this->prefix . $slidedeck['options']['arrow-style'];
            
            /**
             * If we're on the three wide layout, then we have to get the
             * closest slide size relative to the deck sizes.
             */
            if( $slidedeck['options']['layout_style'] == 'layout-three-wide' ){
                $this->slide_size = $this->get_closest_size_percent( $slidedeck, $this->three_wide_slide_size_percent );
            }else{
                $this->slide_size = $this->get_closest_size_percent( $slidedeck, 1.0 );
            }
            
            // Append the found size to the frame classes.
            if( $this->slide_size )
                $slidedeck_classes[] = 'slide-size-' . $this->slide_size;
            
        }
        return $slidedeck_classes;
    }
    
    /**
     * Removing the background-image inline style from the slide DD element
     * Background image is being used within the template on an internal element
     *
     * @param array $slides Array of Slides
     * @param array $slidedeck The SlideDeck object being rendered
     * 
     * @uses SlideDeckLens_Scaffold::is_valid()
     * 
     * @return array
     */
    function slidedeck_get_slides( $slides, $slidedeck ){
        if( $this->is_valid( $slidedeck['lens'] ) ){
            
            foreach( $slides as &$slide ){
                $slide['styles'] = preg_replace("#background-image:\s?url\([^\)]+\);\s?#", '', $slide['styles']);
            }
        }
        return $slides;
    }
}
