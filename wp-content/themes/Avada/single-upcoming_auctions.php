<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="jumbotron" style="background-image:url(<?php the_field('featured_image'); ?>)">
    <div class="banner_text">
        <h1><?php the_field('listing_name'); ?></h1>
        <p>Auction by: <span class="big">Maltz Auctions</span></p>
        <div class="action_buttons" id="split">
            <div>
               <button class="btn_small">Follow</button> 
                
                <?php echo do_shortcode('[favorite_button]'); ?>
            </div>
            <div>
                <a href="/contact">
                    <button class="btn_small">Inquire</button> 
                </a>
            </div>
        </div>
            <div>
                <?php echo do_shortcode('[addtoany]'); ?>
                
            </div>
    </div>
</div>
<div class="four_pillars" id="sticker">
    <div>
        <p id="auction_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Auction Info</p>
    </div>
        <div>
        <p id="photos_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Photos</p>
    </div>
        <div>
        <p id="property_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Property Info</p>
    </div>
        <div>
        <p id="location_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Location</p>
    </div>
</div>

        <div class="about_us_snippets auction_snippets" id="auction_release">
            <div class="snippet">
                <div class="featured_in_header">
                     <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_date.png">
                </div>
                <h3>Auction Date & Time</h3>
                <p><?php the_field('complete_auction_date_&_time'); ?></p>
                <p><strong>Viewing/Inspection: </strong><?php the_field('auction_viewings'); ?></p>
            </div>
            <div class="snippet">
                <div class="featured_in_header">
                    <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_price.png">
                </div>
                <h3>Price Info</h3>
                <p><strong>_______</strong></p>
            </div>
             <div class="snippet">
                <div class="featured_in_header">
                    <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_marker.png">
                </div>
                <h3>Property Address</h3>
                 <p><?php the_field('property_location'); ?></p>
                 <p><?php the_field('property_address'); ?></p>
            </div>
            
            <div class="snippet_bottom">
                <h6>Registration commences at <?php the_field('registration_time'); ?></h6>
                <p>Online & absentee bidding available with registration at least 5 business days prior to auction, please call for details. </p>
            </div>
        </div>

<div class="auction_photos" id="photos_release">
        <div class="standard_contain">
		<div class="featured_in_header">
			<h1><span>Photos</span></h1>
		</div>
        <?php the_field('gallery'); ?>
<!--        <//?php echo do_shortcode('	[URIS id=710594]'); ?>-->
    </div>
</div>

<div class="auction_info" id="property_release">

    <div class="standard_contain">
        		<div class="auction_info_header">
			<h1><?php the_field('listing_name'); ?></h1>
		</div>
            
        <div class="auction_info_subheader">
            <p><?php the_field('auction_info'); ?></p>
		</div>
        
        <div class="auction_info_content">

<?php the_field('property_info'); ?>

</div>
    </div>





 
</div>


<div class="about_us_section" id="location_release">
    <div class="standard_contain">


		<div class="featured_in_header">
			<h1><span>Additional Information</span></h1>
		</div>

		<div class="about_us_text">
            
        <div class="action_buttons" id="split">
            <div>
               <button class="btn_small">Terms</button> 
            </div>
            <div>
               <button class="btn_small">Rights</button> 
            </div>
        </div>
		</div>
	</div>
	
</div>


<div class="property_map">
<?php the_field('property_map'); ?>
</div>
<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>


<script type="text/javascript">

$(document).ready(function(){
  jQuery('.slick_slider').slick({
  	        dots: false,
        infinite: true
  });
});
    
    $(".auction_holder").hover(function(){
    $(".overlay",this).slideToggle(500);
        
});
		
</script>

<script>
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:100});
  });
</script>

<script>
    $("#auction_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#auction_release").offset().top
    }, 2000);
});
    $("#property_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#property_release").offset().top
    }, 2000);
});
$("#photos_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#photos_release").offset().top
    }, 2000);
});
    $("#location_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#location_release").offset().top
    }, 2000);
});
</script>

<script>
(function ($, root, undefined) {


   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 85;
			 if ($(window).scrollTop() > navHeight) {
				 $('.four_pillars').addClass('fixed');
			 }
			 else {
				 $('.four_pillars').removeClass('fixed');
			 }
		});
	});

   })(jQuery, this);

</script>

<?php get_footer(); ?>