=== SlideDeck 3 Professional Addon for WordPress ===
Contributors: SlideDeck
Tags: Slider, dynamic, slide show, slideshow, widget, Search Engine Optimized, seo, jquery, plugin, pictures, slide, skinnable, skin, posts, video, photo, media, image gallery, iPad, iphone, vertical slides, touch support, theme, Facebook, NextGEN Gallery
Requires at least: 3.3
Tested up to: 4.7
Stable tag: trunk

Create SlideDecks on your WordPress blogging platform. Manage SlideDeck content and insert them into templates and posts.

== Description ==

The SlideDeck 3 Developers Addon adds the ability to edit lenses, copy lenses as well as the ability to do raw HTML custom slides for your Custom SlideDecks. 

**Requirements:** PHP5+, WordPress 3.3+, SlideDeck 3 Personal and SlideDeck 3 Professional

**Important Links:**

* [More Details](https://www.slidedeck.com/)
* [Knowledge Base](https://www.slidedeck.com/docs/)
* [Support](http://support.slidedeck.com/)

== Changelog ==
= 4.3 =
Bug Fix: Classic Lens responsive bug fixed for smaller devices (max-width:320px).

= 4.2 =
  Updated version number to keep parity with core

= 4.1 =
  Updated version number to keep parity with core

= 4.0.5 =
 Feature Enhancement: LayerPro Lens - Added Video Background.

= 4.0.4 =
  Updated version number to keep parity with core

= 4.0.3 =
  Updated version number to keep parity with core .

= 4.0.2 =
  Updated version number to keep parity with core .

= 4.0.1 =
 Bug Fix: Navigation bug fix for mobile view of Classic lens.

= 4.0 =
  Updated version number to keep parity with core .

= 3.9 =
  Updated version number to keep parity with core .

= 3.8 =
  Updated version number to keep parity with core .

= 3.7 =
  Updated version number to keep parity with core .

= 3.6.1 =
  Updated version number to keep parity with core .

= 3.6 =
  Updated version number to keep parity with core .

= 3.5 =

 Feature Enhancement: Added Support for HTML5 video. 
 
= 3.4.9 =
  Updated version number to keep parity with core .

= 3.4.8 =
  Updated version number to keep parity with core.
 
= 3.4.7 =
  New Feature: Added new source Zenfolio. 
  Now one can show tumblr gallery or collection photos on Slider.

= 3.4.6 =
  New Feature: Added new source tumblr. 
  Now one can show tumblr blog photos or blog videos on Slider. 

= 3.4.5 =
   Updated version number to keep parity with core.

= 3.4.4 =
  New Feature: Added new option as Spine Position has been added. Now one can have horizontal spines in Classic Lens.

= 3.4.3 =
   Updated version number to keep parity with core.

= 3.4.2 =
   Updated version number to keep parity with core.

= 3.4.1 =
   Bug Fix: If image is not available within SlideDeck contents then, Slider Contents are not getting displayed within sliders.

= 3.4.0 =
   Updated version number to keep parity with core.

= 3.3.6 =
   Updated version number to keep parity with core.

= 3.3.5 =
   Updated version number to keep parity with core.

= 3.3.4 =
   Feature enhancement: Classic lens and custom textOnly and video slideDeck will support to Enhanced 'Click to Acton (CTA)' feature.

= 3.3.3 =
   Updated version number to keep parity with core.

= 3.3.2 =
   Bug Fix: Issue related to flicering spines on mouse hover.

= 3.3.1 =
  Updated version number to keep parity with core
  
= 3.3 =
  New Feature : Added Click to Action (CTA) button option for the classic lens and custom lens.

= 3.2.6 =
  Bug Fix: In classic Lens, if 'Show Slide Controls' option is set to 'Always' spines are now showing properly.

= 3.2.5 =
  Updated version number to keep parity with core

= 3.2.4 =
  Updated version number to keep parity with core
  
= 3.2.3 =
  Updated version number to keep parity with core

= 3.2.2 =
  Updated version number to keep parity with core

= 3.2.1 =
  Updated version number to keep parity with core

= 3.2.0 =
  
  Bug Fix: Custom css editor will not break after full scrolling.
  Bug Fix: Lazy loading in custom is fixed.
  
= 3.1.1 =
  Added changes to support slidedeck scheduler addon

= 3.1.0 =
 New Feature : Facebook Access Token not required to be regenerated every 60 days 

= 3.0.0 =
* Initial beta release.
