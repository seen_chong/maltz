<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

// Dev
// define('DB_NAME', 'maltz_wp');
// define('DB_USER', 'root');
// define('DB_PASSWORD', 'root');

// define('DB_HOST', 'localhost');

// define('DB_CHARSET', 'utf8');

// define('DB_COLLATE', '');

// define('WP_HOME','http://maltzauctions.dev/');
// define('WP_SITEURL','http://maltzauctions.dev/');


// Live
define('DB_NAME', 'maltzmym_wp');
define('DB_USER', 'maltzmym_user');
define('DB_PASSWORD', 'RX0B9vn;D7tL');

define('DB_HOST', 'localhost');

define('DB_CHARSET', 'utf8');

define('DB_COLLATE', '');

define('WP_HOME','http://maltz.mymagid.net');
define('WP_SITEURL','http://maltz.mymagid.net');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
// define('AUTH_KEY',         'GG$D%G/e+H4<zB5:|R~yKlq<zhcs?Wh+PojPYkemWkZ|+$wcgIw[TBG):<6V+#+1');
// define('SECURE_AUTH_KEY',  '3TGAo3&M9PM-5iDCI;Xj0pQQ{V-FfoQy-f+T0A(|{YX%[pe-Fv1:E-W/:r-a!f*n');
// define('LOGGED_IN_KEY',    '(W(Z5r;t=e%GEKXX}#KpgN/zOO0Qp,]O[%E=%?@7@6Z-B[G%R2j}]{-kPC2{)(``');
// define('NONCE_KEY',        '`Rtn4QRgJejhDO,lQzKDm&{d-IonyML5w(7z52uw-}|C-t1cb7m@nh+.eL-_$Xk+');
// define('AUTH_SALT',        'NS4SXTo+Z<eK@7RI7L89:x5{P_ea:&pn>-AE:ic%-m`nv&@Xu>Bn5ugQ_(b>+qUf');
// define('SECURE_AUTH_SALT', 'K?&3R9Sy+(kF|R-q|T66=2#{0#SEXedDhZrPX @D`];X9[xZ>4Rq!Zb2_&YVp/{@');
// define('LOGGED_IN_SALT',   'F~p+&x3Y[jBm5qi^{=Z)2->0YK:(N]M/%IdSW+-fS_w%I{j[I_a^8<V9Bv+B,k_4');
// define('NONCE_SALT',       'bc]`I|0UjB6RC90j&;Ftx#6e9_D359U-&Y_||U+OO>2*ak2tNc5MoY#cv|82s{vb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Setup 'direct' method for Wordpress, auto update without FTP */
define('FS_METHOD', 'direct');
define('FS_CHMOD_DIR', 0775);
define('FS_CHMOD_FILE',0664);	

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
