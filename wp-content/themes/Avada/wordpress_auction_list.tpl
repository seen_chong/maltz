<!-- BEGIN AUCTION LIST TEMPLATE -->
<!-- BEGIN #acListWrap -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
{literal}
<style>
div.auctionListingLeft {
    float: none;}
#auction_title{background:none;}'

'#auction_title a{background:none;font:}

div.auctionListingMiddle {overflow:hidden!important;
    margin-top: 1px;}


div.auctionListing {
    background: none;
border-radius: 10px;}

div.auctionListLinks {
    border: 0 none;
display: none;}
.main_img img {
    box-shadow: none;
    height: 192px;
    margin: 3px 0 10px;
width: 256px;}
.auctionList {
    background: none repeat scroll 0 0 #f1f1f1;    border-radius: 0;    float: left;    margin-right: 12px;    margin-top: 12px;    width: 31.5%;
    border: 2px solid #346699;
    border-radius: 10px;
    height: 350px;
    overflow: hidden;
}
.acListEventBiddingLink{display:none;}
#listSubtitle{}
#content-full {
    margin-top: 20px;
}
#acListWrap {
    font: 10px/15px arial; margin: 0px 0 0;
}
.auctionList h1 {
    border-bottom: 1px solid #cccccc;
    font: 12px/16px arial;
    padding: 0px 6px 6px;
}
.auctionList h1 a{color:#346699;}
.auctionList h1 a:hover{color:#77a6d1;}

.shortdesc p {margin:none;padding:0px;font:11px/12px arial;color:#222}
#summarySubtitle p{font:bold 15px arial;padding:0px;margin:0px;}
</style>
{/literal}




<div id="acListWrap">

<!-- AD BLOCK-->
<div style="text-align:center;font:11px/13px arial;background: none repeat scroll 0 0 #fff; border: 2px solid #346699; border-radius: 10px; float: left; height: 350px; margin-right: 12px; margin-top: 12px; overflow: hidden; width: 31.5%;">
<br />
<img style=""  src="http://new.motleys.com.php53-15.dfw1-1.websitetestlink.com/wp-content/uploads/2015/02/Image35.png" /> <br />
<div style="padding:5px 10px;color:#333;">
 Motley's is a full-service operation ready to assist you with the sale of your real estate property. Our professional staff has the depth of knowledge and experience to help you obtain the best value for your property.
<br /><br />
Motley's tailors each auction event to our clients and their properties. Contact us today to discuss how we can help you with our many services.
</div></div>

<!-- end AD BLOCK-->


<!-- END TOP PAGINATION --><!-- BEGIN AUCTION LIST -->
{foreach from=$search.auction item=auction}
	<div class="auctionList">
		
<!-- BEGIN AUCTION -->
		<div class="auctionListing">
<!-- BEGIN #auctionListingLeft -->
			<div class="auctionListingLeft">
<!-- BEGIN LIST IMAGE -->
				<div class="main_img">					
					<a href="/auction/{$auction.id}/{$auction.title|urlize|lower}/" title="{$auction.title}">
						{if $auction.image}
							<img src="{$auction.image[0].medium_url}" alt="{$auction.title}" />
						{else}
							<img src="{$assets_url}images/no_image160.jpg" alt="{$auction.title}" />
						{/if}
					</a>
					{if $auction.is_sold eq 'true' and $auction.is_closed eq 'true'}
						<div class="acListState">{#soldAndClosedText#}</div>
					{else}
					{if $auction.is_sold eq 'true'}
							<div class="acListState">{#soldText#}</div>
					{/if}
						{if $auction.is_closed eq 'true'}
							<div class="acListState">{#closedText#}</div>
						{/if}
					{/if}
				</div>
<!-- END LIST IMAGE --> <!-- BEGIN LINKS SECTION -->
				
			</div>
			<h1 id="auction_title" class="">
			<a href="/auction/{$auction.id}/{$auction.title|urlize|lower}" title="{$auction.title}">{$auction.title}</a>
		</h1>
<!-- END #auctionListingLeft --><!-- BEGIN #auctionListingMiddle -->
			<div class="auctionListingMiddle">
<!-- BEGIN #upperSection -->
			
<!-- END #upperSection -->
<!-- BEGIN #lowerSection -->
<div id="lowerSection" class="" style="margin-top:5px; border:0px">	
<div class="shortdesc" style="border:none;">
					<p>{$auction.short_description}</p>
				</div>			
<!-- END EVENT TYPE, START DATE TIME AND END DATE TIME -->	
		                  
			</div><!-- END #lowerSection -->		
		</div><!-- END #auctionListingMiddle -->{if $auction.subtitle}
			<div id="summarySubtitle">
			<h2 style="color:#000;font:16px arial;background:#fff;padding:10px 0">{$auction.subtitle}</h2>
			</div>
		{/if}		
	</div><!-- END AUCTIONS -->	
</div><!-- END AUCTION LIST -->
<!-- BEGIN NO AUCTIONS -->	
{foreachelse}
  <h2 class="noAuctions">{#noAuctionsText#}</h2>
{/foreach}
<!-- END NO AUCTIONS -->

</div><!-- END #acListWrap -->

<!-- BUTTONIZE OUR BUTTONS -->
{literal}<script>jQuery(function(){jQuery('#paginationTop').buttonset();jQuery('#paginationBottom').buttonset();});</script>{/literal}
{literal}<script>jQuery(function(){jQuery("a",".button-links").button();});</script>{/literal}
<!-- END AUCTION LIST TEMPLATE -->