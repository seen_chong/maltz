<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="services_tab">
    <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
        <h6>Auctions</h6>
    </div>
        <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/appraisals_icon.png">
        <h6>Appraisals</h6>
    </div>
        <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/storage_icon.png">
        <h6>Storage</h6>
    </div>
        <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/sales_icon.png">
        <h6>Brokered Sales</h6>
    </div>
        <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/equity_icon.png">
        <h6>Equity Advance</h6>
    </div>
        <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <h6>Receivership</h6>
    </div>
            <div>
        <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
        <h6>UCC / Mezzanine / Security Agreement</h6>
    </div>
</div>

<div class="section_services">
    <div class="standard_contain">


		<div class="featured_in_header">
            <div class="blue_bubble">
			     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_gavel.png">
                </div>
             <h1 ><span>Auctions</span></h1>
		</div>

		<div class="about_us_text">
            <p>An auction is a highly effective and innovative method of selling a variety of assets including real estate, personal property and intellectual property. Auctions facilitate the rapid sale of assets while obtaining market value through the benefit of an accelerated marketing plan and generation of a competitive bidding atmosphere.</p>
            
            <p>While traditional asset listings place a limit on the maximum attainable sales price by setting a price ceiling (asking price), auctions eliminate price ceilings and realize maximum selling prices by creating an upward bid swing with the sky as the limit.</p>
            
            <p>Maltz Auctions’ has literally conducted thousands of auctions featuring a vast array of assets ranging from automobiles and aircraft to real estate and construction equipment.</p>
            
            <p>When our company is retained to conduct an auction, our clients find comfort in knowing that we take care of everything involved in the sale from compiling due diligence materials and creating marketing campaigns to providing professional auction day staff and collection of funds.  We truly are a full service auction company.</p>

		</div>
        
	</div>
	
</div>

<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Receive Weekly Updates via Email</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>