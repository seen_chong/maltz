<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/jumbo_about.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">About Us</h1>
    </div>
</div>


<div class="about_us_section">
    <div class="standard_contain">


		<div class="featured_in_header">
            <div class="blue_bubble">
			 <img src="/wp-content/themes/Avada-Child-Theme/images/icon_gavel.png">
                </div>
		</div>

		<div class="about_us_text">
            <p>Our 35-year-old firm is nationally recognized as a trusted name in excellence and achievement. Maltz Auctions is a premier, full-service auction company specializing in creating liquidity through the auction method of marketing. Now a $100+ million company with an impeccable reputation for professionalism, integrity and success, Maltz Auctions has conducted thousands of auctions and generated over $2 billion in sales on behalf of such clients as the U.S. Bankruptcy Courts and financial institutions throughout the country, the U.S. Department of Justice, the Securities and Exchange Commission, the Internal Revenue Service, N.Y. State Department of Taxation & Finance, Court Appointed Receivers, Estate Executors and Public Administrators, as well as law firms, private individuals and corporations, nationwide. Maltz Auctions is court approved to act in the capacity of auctioneers, real estate brokers and receivers.</p>
            
            <h3>Get In on the Action.</h3>
            
            <div class="holder_btn">
                <a href="">
                    <button class="btn_classic" id="extend_300">Upcoming Auctions</button>
                </a>
            </div>
		</div>
        
	</div>
	
</div>

        <div class="about_us_snippets">
            <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_gavel.png">
                    </div>
                </div>
                <h3>BUYERS</h3>
                <p>A broker-loyal partnership</p>
                <p>Award-winning marketing</p>
                <p>On-site sales support</p>
                <p>Commissions protected</p>
            </div>
            <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_gavel.png">
                    </div>
                </div>
                <h3>SELLERS</h3>
                <p>Accelerated sales solution</p>
                <p>Global exposure</p>
                <p>Industry leading database</p>
                <p>Market value on your timeleine</p>
            </div>
             <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_gavel.png">
                    </div>
                </div>
                <h3>FIDUCIARIES</h3>
                <p>Curated inventory</p>
                 <p>Easy bidding process</p>
                 <p>Your price</p>
                 <p>Smooth, simple transaction</p>
            </div>
        </div>



<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Receive Weekly Updates via Email</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>