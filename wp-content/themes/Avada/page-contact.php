<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/skyline_mini.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">Contact</h1>
    </div>
</div>

<div class="about_us_section">
        <div class="about_us_snippets" id="page_contact">
            <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_mail.png">
                    </div>
                </div>
                <h3>EMAIL</h3>
                <p>info@MaltzAuctions.com</p>
            </div>
            <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_phone.png">
                    </div>
                </div>
                <h3>NUMBER</h3>
                <p><strong>p:</strong> (516) 349-7022</p>
                <p><strong>f:</strong> (516) 349-0105</p>
            </div>
             <div class="snippet">
                <div class="featured_in_header">
                    <div class="blue_bubble">
                     <img src="/wp-content/themes/Avada-Child-Theme/images/icon_pin.png">
                    </div>
                </div>
                <h3>LOCATION</h3>
                <p>39 Windsor Place</p>
                 <p>Islandia, NY 11722</p>
            </div>
        </div>
</div>

<div class="section_contact">
    <div class="standard_contain">
        <div class="left">
           <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]'); ?>
        </div>

        <div class="right">
<iframe height="500px" width="100%" border="0" marginwidth="0" marginheight="0" src="https://www.mapquest.com/embed/us/new-york/business-central-islip/maltz-auctions-355385709?center=40.79306299999998,-73.179294&zoom=15&maptype=undefined"></iframe>
        </div>
    </div>
</div>
 



<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>




<?php get_footer(); ?>