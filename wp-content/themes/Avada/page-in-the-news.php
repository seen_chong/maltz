<?php get_header(); ?>


<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/news_banner.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">In The News</h1>
    </div>
</div>

<div class="standard_contain ">
	<div class="contact_section">	
					<div class="featured_in_header">
			<h1><span>Latest from Maltz Auctions</span></h1>
		</div>
		<div class="news_posts">
			<?php echo do_shortcode('[display-posts include_date="true" date_format="F j, Y" include_excerpt="true" excerpt_length="30" excerpt_more="Continue Reading" excerpt_more_link="true"] '); ?>
		</div>
	
	</div>
</div>

<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Receive Weekly Updates via Email</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>

<div class="more_offers" style="display:none;">
<div class="wrapper">
<h2>We offer more than premium real estate.</h2>
<h4>Let’s look into some of our other categories below</h4>
		<div>
			<a href="/upcoming-auctions/jewelry-collectibles/">
				<img src="/wp-content/uploads/2014/12/more.jpg" alt="" width="493" height="411" class="alignnone size-full wp-image-710269" />
				<p>Jewelry & Collectibles</p>
			</a>
		</div>

		<div>
			<a href="/upcoming-auctions/unique-assets/">
				<img src="/wp-content/uploads/2014/12/more2.jpg" alt="" width="494" height="411" class="alignnone size-full wp-image-710271" />
				<p>Unique Assets</p>
			</a>
		</div>

		<div>
			<a href="/upcoming-auctions/autos-boats/">
				<img src="/wp-content/uploads/2014/12/more3.jpg" alt="" width="492" height="411" class="alignnone size-full wp-image-710272" />
				<p>Autos & Boats</p>
			</a>
		</div>
</div>
</div>


<?php get_footer(); ?>