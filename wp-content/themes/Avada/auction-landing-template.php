<?php
/*
 * Template Name: Auction Landing
 */

get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>





<div class="upcoming_auctions">
		<div class="featured_in_header">
            <img src="/wp-content/themes/Avada-Child-Theme/images/icon_cal.png">
			<h1><span><?php the_title(); ?></span></h1>
		</div>

		<div class="auction_holdings">

            
                        <?php
            $args = array(
      'post_type' => 'upcoming_auctions'       
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            <a href="<?php the_permalink(); ?>">
                <div class="auction_holder" id="row_3" style="background-image:url(<?php the_field('featured_image'); ?>);">
                    <div class="overlay">
                        <div class="auction_holder_contain">
                            <div class="auction_detail">
                                <h4><?php the_field('listing_name'); ?></h4>
                                <p><?php the_field('city/state'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
                        <?php
            }
                  }
            else  {
            echo 'No Auctions Found';
            }
      ?> 

        </div>  
</div> 



<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred_1.jpg);">
	<h4>Maltz Auction Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>