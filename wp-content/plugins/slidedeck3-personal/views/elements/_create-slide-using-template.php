<div id="extra-new-slide-template-div"></div>
<div id="create-new-slider-template-slidedeck" class="create-slidedeck">
	 <div class="slidedeck-inner">	
	 	<h4>Templates</h4>
	    <p>Create a slider using Predefined Template.</p>	    
	    <p><a href="<?php echo admin_url( "admin-ajax.php?action=slidedeck_template_modal&_wpnonce_template_modal=" ) . wp_create_nonce( 'slidedeck-template-modal' ); ?>" class="button create-button slidedeck-source-modal" onclick="return false;"><span><?php _e( "Create SlideDeck", $this->namespace ) ?></span></a></p>
	 		 
	</div>
</div>
