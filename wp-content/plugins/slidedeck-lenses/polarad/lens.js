(function($){
	SlideDeckLens['instant_film'] = function(slidedeck){
        var slidedeck = $(slidedeck);
        var slidedeckFrame = slidedeck.closest('.slidedeck-frame');
        var deck = slidedeck.slidedeck();
        var deckElement = slidedeck;

        // Only for IE - detect background image url and update style for DD element
        if( ie <= 8.0 ){
            deck.slides.each(function(ind){
                var imageDiv = $(deck.slides[ind]).find('div.image');
                var shadowDiv = $(deck.slides[ind]).find('div.sd2-shadow-element');
                var tapeDiv = $(deck.slides[ind]).find('div.sd2-tape-element');
                if( imageDiv.css('background-image') != 'none' ){
                    var imgurl = imageDiv.css('background-image').match( /url\([\"\'](.*)[\"\']\)/ )[1];

                    // IE8 has issues with background image AND relative paths...
                    shadowDiv.css({
                        background: 'none'
                    });
                    tapeDiv.css({
                        background: 'none'
                    });

                    imageDiv.css({
                        background: 'none'
                    });
                    imageDiv[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgurl + "', sizingMethod='scale')";
                };
            });
        }
        
        // For stacked layout
        if( slidedeckFrame.hasClass( 'layout-stacked' ) ){
            deck.slides.each(function(ind){
                var slide = deck.slides.eq(ind).find('.slide-content');
                
                var rotate = Math.round(Math.random() * 2.5);
                if(Math.round(Math.random()) == 0){
                    rotate = 0 - rotate;
                }
                
                var slideCSS = {};
                    slideCSS['-webkit-transform-origin'] = '50% 50%';
                    slideCSS['-moz-transform-origin'] = '50% 50%';
                    slideCSS['-o-transform-origin'] = '50% 50%';
                    slideCSS['transform-origin'] = '50% 50%';
                    slideCSS['-webkit-transform'] = 'rotate3d(0,0,0,' + rotate + 'deg) scale3d(0.8,0.8,0.8)';
                    slideCSS['-moz-transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
                    slideCSS['-o-transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
                    slideCSS['transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
                
                slide.css(slideCSS);
                
            });
        }
        
        // rotate tape
        deck.slides.each(function(ind){
            var tape = deck.slides.eq(ind).find('.sd2-tape-element');
            
            var rotate = Math.round(Math.random() * 4);
            if(Math.round(Math.random()) == 0){
                rotate = 0 - rotate;
            }
            
            var tapeCSS = {};
                tapeCSS['-webkit-transform-origin'] = '50% 50%';
                tapeCSS['-moz-transform-origin'] = '50% 50%';
                tapeCSS['-o-transform-origin'] = '50% 50%';
                tapeCSS['transform-origin'] = '50% 50%';
                tapeCSS['-webkit-transform'] = 'rotate3d(0,0,0,' + rotate + 'deg) scale3d(0.8,0.8,0.8)';
                tapeCSS['-moz-transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
                tapeCSS['-o-transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
                tapeCSS['transform'] = 'rotate(' + rotate + 'deg) scale(0.8)';
            
            tape.css(tapeCSS);
            
        });
        
        var oldBefore = deck.options.before;
        deck.options.before = function(){
            // If the old before option was a function, run it
            if(typeof(oldBefore) == 'function')
                oldBefore(deck);
            
            addClasses( deck );

            if( !$.data(slidedeckFrame, 'added-transitions') ){
                $.data(slidedeckFrame, 'added-transitions', true );
                var transitionSpeed = deck.options.speed / 4;
                
                deck.slides.find('.slide-content, .paper').css({
                    '-webkit-transition': 'all ' + transitionSpeed + 'ms cubic-bezier(0.000, 0.000, 0.230, 0.995)',
                    '-moz-transition': 'all ' + transitionSpeed + 'ms cubic-bezier(0.000, 0.000, 0.230, 0.995)',
                    '-o-transition': 'all ' + transitionSpeed + 'ms cubic-bezier(0.000, 0.000, 0.230, 0.995)',
                    'transition': 'all ' + transitionSpeed + 'ms cubic-bezier(0.000, 0.000, 0.230, 0.995)',
                });
                
            }
        }
        
        deck.loaded( function(){
            addClasses( deck );
            fixHeight();
        });
        
        var fixHeight = function() {
            if( slidedeckFrame.hasClass( 'layout-stacked' ) ) {
                slidedeckFrame.find('dd .paper .image').css({
                    height: slidedeckFrame.find('dd .paper .image:first').width()
                });
            }
        }
        
        var addClasses = function( deck ){
            // Add some classes to the slides that are before and after
            deck.slides.each(function(ind){
                var current = deck.current - 1;
                if( ind < current ){
                    $(deck.slides[ind]).removeClass('after-slide').addClass('before-slide');
                }else if( ind > current ){
                    $(deck.slides[ind]).removeClass('before-slide').addClass('after-slide');
                }
            });
        }
	};
    
    $(document).ready(function(){
        $('.lens-polarad .slidedeck').each(function(){
            if(typeof($.data(this, 'lens-polarad')) == 'undefined'){
                $.data(this, 'lens-polarad', new SlideDeckLens['instant_film'](this));
            }
        });
    });
})(jQuery);