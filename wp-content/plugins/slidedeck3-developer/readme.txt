=== SlideDeck 3 Developer Addon for WordPress ===
Contributors: SlideDeck
Tags: Slider, dynamic, slide show, slideshow, widget, Search Engine Optimized, seo, jquery, plugin, pictures, slide, skinnable, skin, posts, video, photo, media, image gallery, iPad, iphone, vertical slides, touch support, theme
Requires at least: 3.3
Tested up to: 4.7
Stable tag: trunk

Create SlideDecks on your WordPress blogging platform. Manage SlideDeck content and insert them into templates and posts.

== Description ==

The SlideDeck 3 Developers Addon adds the ability to edit lenses, copy lenses as well as the ability to do raw HTML custom slides for your Custom SlideDecks.

**Requirements:** PHP5+, WordPress 3.3+, SlideDeck 3 Personal and SlideDeck 3 Professional

**Important Links:**

* [More Details](https://www.slidedeck.com/)
* [Knowledge Base](https://www.slidedeck.com/docs/)
* [Support](http://support.slidedeck.com/)


== Changelog ==
= 4.3 =
  Bug Fix: Polarad Lens - responsive bug fixed related to navigation and height for smaller devices.
  Bug Fix: Parallax Lens - navigation arrows changed.
  Bug Fix: Parallax Lens - cycle js conflict resolved.
  Bug Fix: Parfocal Lens - responsive bug fixed for smaller devices.

= 4.2 =
  Updated version number to keep parity with core

= 4.1 =
  Updated version number to keep parity with core

= 4.0.5 =
  Updated version number to keep parity with core

= 4.0.4 =
  Updated version number to keep parity with core

= 4.0.3 =
  Updated version number to keep parity with core

= 4.0.2 =
  Updated version number to keep parity with core

= 4.0.1 =
  Updated version number to keep parity with core

= 4.0 =
  Updated version number to keep parity with core

= 3.9 =
  Updated version number to keep parity with core

= 3.8 =
  Updated version number to keep parity with core

= 3.7 =
  Updated version number to keep parity with core

= 3.6.1 =
  Updated version number to keep parity with core

= 3.6 =
  Updated version number to keep parity with core

= 3.5 =
  Updated version number to keep parity with core

= 3.4.9 =
  Updated version number to keep parity with core

= 3.4.8 =
  Updated version number to keep parity with core

= 3.4.7 =
  Updated version number to keep parity with core

= 3.4.6 =
  Updated version number to keep parity with core

= 3.4.5 =
  Updated version number to keep parity with core

= 3.4.4 =
  Updated version number to keep parity with core

= 3.4.3 =
  Updated version number to keep parity with core

= 3.4.2 =
  Updated version number to keep parity with core

= 3.4.1 =
  Updated version number to keep parity with core

= 3.4.0 =
  Updated version number to keep parity with core

= 3.3.6 =
  Updated version number to keep parity with core

= 3.3.5 =
  Updated version number to keep parity with core

= 3.3.4 =
  Updated version number to keep parity with core

= 3.3.3 =
  Updated version number to keep parity with core

= 3.3.2 =
  Updated version number to keep parity with core

= 3.3.1 =
  Updated version number to keep parity with core

= 3.3 =
  Updated version number to keep parity with core

= 3.2.6 =
  Updated version number to keep parity with core

= 3.2.5 =
  Updated version number to keep parity with core

= 3.2.4 =
  Updated version number to keep parity with core

= 3.2.3 =
  Updated version number to keep parity with core

= 3.2.2 =
  Updated version number to keep parity with core

= 3.2.1 =
  Updated version number to keep parity with core

= 3.2.0 =
  Updated version number to keep parity with core

= 3.1.1 =
  Added changes to support slidedeck scheduler addon

= 3.1.0 =
* To keep in parity with personal

= 3.0.0 =
* Initial beta release.
