<?php get_header(); ?>


<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/news_banner.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">My Account</h1>
    </div>
</div>

<div class="member_contain ">
<?php echo do_shortcode('[ultimatemember_account]'); ?>    
</div>


<style>
    .um-account-side {
        margin: 0 auto;
    }
    .um-account-main, .um-account-side {
        float: none;
    }
    .member_contain {
        padding-top: 60px;
        padding-bottom: 60px;
        width: 100%;
        text-align:center;
    }

</style>

<?php get_footer(); ?>