<!-- BEGIN WORDPRESS AUCTION DETAILS TEMPLATE -->
<div id="acDetailWrap">
	<!-- begin auction title -->
	<div class="auctionTitle">
		<h1>{$auction.title}</h1>
		{if $auction.subtitle}
			<div id="summarySubtitle">
			<h2>{$auction.subtitle}</h2>
			</div>
		{/if}                  
	</div>
<!-- end auction title -->
{literal}
<script type="text/javascript">jQuery(function(){jQuery("#topTabs").tabs();});</script>
{/literal}	
	<div id="topTabs">
		<ul>
			<li><a href="#tabs-details"> Main</a></li>
			{if ($auction.image and count($auction.image) > 1) or count($auction.gallery) > 0}<li><a href="#detail-tabs-morephotos"> Photos</a></li>{/if}
			{if $auction.video_link}<li><a href="#detail-tabs-video"> Videos</a></li>{/if}	
			{if $auction.directions or $auction.location}<li><a href="#tabs-directionsmap">Directions</a></li>{/if}
			<li><a href="#tabs-contact"> {#contactText#}</a></li>
			<li><a href="#tabs-print" onclick="return ignite_print('auction', {$auction.id});"></span> {#printAuctionText#}</a></li>
		</ul>
		<div id="tabs-details">		
			<div id="summaryHeader"><!-- Begin summary header -->	
				<div id="summaryHeaderLeft">
					<div class="detail_image">
						<div align="center">
						<!-- begin 5 image fader -->			
							<div class="primaryImageView">
							{if $auction.image}
								{if count($auction.image) > 1}
									<ul id="featuredImageGalleryView" style="display:none;">
									{foreach from=$auction.image item=image}
										<li><img src="{$image.medium_url}" data-frame="{$image.tiny_url}" data-description="{$image.description}" title="{$image.title}" alt="{$image.title}" /></li>
									{/foreach}
									</ul>
{literal}<script type="text/javascript">jQuery('#featuredImageGalleryView').galleryView({show_panels:true,show_panel_nav:true,enable_overlays:false,panel_width:250,panel_height:210,panel_animation:'fade',panel_scale:'fit',pan_images:false,start_frame:1,show_filmstrip:true,show_filmstrip_nav:false,enable_slideshow:true,autoplay:true,show_captions:false,filmstrip_size:3,filmstrip_style:'scroll',filmstrip_position:'bottom',frame_width:40,frame_height:30,frame_opacity:0.4,frame_scale:'crop',frame_gap:5,show_infobar:false});</script>{/literal}
									<br />
								{else}
									{foreach from=$auction.image item=image}
										<div style="margin:10px;">
											<a class='primary-gallery' href='{$image.large_url}' title="{$image.title}"><img src="{$image.medium_url}" title="{$image.title}" alt="{$image.title}" width="240px" /></a>
											</div>
											{/foreach}
{literal}<script type="text/javascript">jQuery('a.primary-gallery').colorbox({rel:'primary-gallery', transition:"none"});</script>{/literal}
								{/if}																
								{else}
								{load_default_image}
								{if $has_default_image}
									<img src="{$default_image}" alt="{$auction.title}" class="defaultImage" width="260px" />
									{#imageUnavailableText#}
								{else}
									<img src="{$assets_url}/images/no_image320.jpg" title="no image" alt="no image" width="260px" /><br />{#imageUnavailableText#}
								{/if}
							{/if}
						</div>
					</div>
				</div>
			</div>			
			<div id="summaryHeaderRight">
				<div class="tabsection" style="border:none">
					<p>{$auction.short_description}</p>
				</div>			
				<!-- begin auction schedule -->

        {if $auction.primary_event > 0}
        <div class="tabsection">
						<!-- primary events, start date time and end date time -->
						{foreach from=$auction.primary_event item=event}
						<div class="acDetailEvent">
						 <div class="acDetailEventLabel"><strong>{$event.label|upper}</strong></div>
							<div class="acDetailEventInfo">
							  {to_date assign=starting_at date=$event.starting_at}										
                
                {if $starting_at lte $smarty.now}
								<span><strong>{#beganText#}:</strong></span>
								{else}
								<span><strong>{#beginsText#}:</strong></span>
								{/if}
								{$event.starting_at|custom_date_format:"%D"} at {$event.starting_at|custom_date_format:"%l:%M %p"} {$event.zone_short}
               							
								{if $event.ending_at}
                <br />
								{to_date assign=ending_at date=$event.ending_at}			
							  
                {if $ending_at lte $smarty.now}
								<span><strong>{#endedText#}:</strong></span>
								{else}
								<span><strong>{#endsText#}:</strong></span>
								{/if}
								{$event.ending_at|custom_date_format:"%D"} at {$event.ending_at|custom_date_format:"%l:%M %p"} {$event.zone_short}
               				
								{/if}
                
								<!-- address -->
								{if $event.location}
								{assign var=location value=$event.location}
									 {if $event.label == 'Live Off-Site Auction' or $event.label == 'Live Off-site Auction with Online bidding'}
                    <br /><span><strong>{#locationTextAuction#}:</strong><a href="https://maps.google.com/maps?q='{$auction.location.first_address} + {$auction.location.city} + {$auction.location.state.abbreviation} + {$auction.location.zipcode}'" target="_blank"> <img style="vertical-align:bottom;" src="{$assets_url}/images/pin_icon.png" /> </a> </span>{if $auction.location.first_address} {$auction.location.first_address},
                        {if $auction.location.second_address}
    										{$auction.location.second_address},
    									{/if}
    									{if $auction.location.third_address}
    										{$auction.location.third_address},
    									{/if}
    									{if $auction.location.city}
    										{$auction.location.city},
    										{if $auction.location.state}
    											{$auction.location.state.abbreviation}
    										{else}
    											{$auction.location.country.name}
    										{/if}
    									{/if}
    									{if $auction.location.zipcode}
    										{$auction.location.zipcode}
    									{/if}<br />
                    <span><strong>{#locationTextProperty#}:</strong><a href="https://maps.google.com/maps?q='{$location.first_address} + {$location.city} + {$location.state.abbreviation} + {$location.zipcode}'" target="_blank"> <img style="vertical-align:bottom;" src="{$assets_url}/images/pin_icon.png" /> </a> </span>{if $location.first_address} {$location.first_address},
                    {/if}
                  {/if}
                   {elseif $event.label}
                    <br /><span><strong>{#locationText#}:</strong><a href="https://maps.google.com/maps?q='{$location.first_address} + {$location.city} + {$location.state.abbreviation} + {$location.zipcode}'" target="_blank"> <img style="vertical-align:bottom;" src="{$assets_url}/images/pin_icon.png" /> </a> </span>{if $location.first_address} {$location.first_address},
                   {/if}
                  {/if}
									{if $location.second_address}
										{$location.second_address},
									{/if}
									{if $location.third_address}
										{$location.third_address},
									{/if}
									{if $location.city}
										{$location.city},
										{if $location.state}
											{$location.state.abbreviation}
										{else}
											{$location.country.name}
										{/if}
									{/if}
									{if $location.zipcode}
										{$location.zipcode}<br />
									{/if}
								{/if}
                
                <!-- online bidding links -->
									{if $event.label eq "Live On-Site Auction with Online Bidding"}
									  {foreach from=$auction.online_bidding_link item=link}
                    <div class="acListEventBiddingLink">
                    <a href="{$link.url}" target="_blank"><img src="{$assets_url}/images/online.gif" alt="{#bidOnlineNowText#}" /> {#bidOnlineNowText#}</a>
									  </div>
                    {/foreach}
									{/if}
                  {if $event.label eq "Live Off-site Auction with Online bidding"}
									  {foreach from=$auction.online_bidding_link item=link}
                    <div class="acListEventBiddingLink">
                    <a href="{$link.url}" target="_blank"><img src="{$assets_url}/images/online.gif" alt="{#bidOnlineNowText#}" /> {#bidOnlineNowText#}</a>
									  </div>
                    {/foreach}
									{/if}
									{if $event.label eq "Online Bidding Only"}   
  								  {foreach from=$auction.online_bidding_link item=link}
                    <div class="acListEventBiddingLink">
                    <a href="{$link.url}" target="_blank"><img src="{$assets_url}/images/online.gif" alt="{#bidOnlineNowText#}" /> {#bidOnlineNowText#}</a>
  								  </div>
                    {/foreach}
									{/if}
									{if $event.label eq "Online Pre-Bidding"} 
									  {foreach from=$auction.online_pre_bidding_link item=link}
                    <div class="acListEventBiddingLink">
                    <a href="{$link.url}" target="_blank"><img src="{$assets_url}/images/online_prebidding.gif" alt="{#preBidOnlineNowText#}" /> {#preBidOnlineNowText#}</a>
									  </div>
                  {/foreach}
									{/if}
                <!-- online bidding links -->
							</div>
						</div>
						{/foreach}					
						<div id="acDetailSecondaryEvents">
						<!-- secondary events, start date time and end date time -->
						{foreach from=$auction.secondary_event item=event}
							<div class="acDetailEvent">
								<div class="acDetailSecondaryEventLabel"><strong>{$event.label|upper}</strong></div>
								<div class="acDetailSecondaryEventInfo">
									{to_date assign=starting_at date=$event.starting_at}
									
									{if $starting_at lte $smarty.now}
									<span><strong>{#beganText#}:</strong></span>
									{else}
									<span><strong>{#beginsText#}:</strong></span>
									{/if}
									  {$event.starting_at|custom_date_format:"%D"} at {$event.starting_at|custom_date_format:"%l:%M %p"} {$event.zone_short}
								    
										{if $event.ending_at}
									  {to_date assign=ending_at date=$event.ending_at}
									
									{if $ending_at lte $smarty.now}<br />
									<span><strong>{#endedText#}:</strong></span>
									{else}
									<span><strong>{#endsText#}:</strong></span>
									{/if}
										  {$event.ending_at|custom_date_format:"%D"} at {$event.ending_at|custom_date_format:"%l:%M %p"} {$event.zone_short}
									{/if}
										{if $event.location}
										{assign var=location value=$event.location}
									 <br /><span><strong>{#locationText#}:</strong><a href="https://maps.google.com/maps?q='{$location.first_address} + {$location.city} + {$location.state.abbreviation} + {$location.zipcode}'" target="_blank"> <img style="vertical-align:bottom;" src="{$assets_url}/images/pin_icon.png" /> </a> </span>{if $location.first_address} {$location.first_address},
										{/if}
										{if $location.second_address}
											{$location.second_address},
										{/if}
										{if $location.third_address}
											{$location.third_address},
										{/if}
										{if $location.city}
											{$location.city},
											{if $location.state}
												{$location.state.abbreviation}
											{else}
												{$location.country.name}
											{/if}
										{/if}
										{if $location.zipcode}
											{$location.zipcode}
										{/if}
									{/if}
								</div>
							</div><br />
						{/foreach}
						</div>
					</div>
					{/if}

			  <!-- end auction schedule -->					
				</div>
			</div><!-- end summary header -->
		<br style="clear:both;" />
			{literal}<script type="text/javascript">jQuery(function(){jQuery("#detail-tabs").tabs();});</script>{/literal}
		<!-- Detail inner tabs --> 
			<div id="detail-tabs">
				<ul>
					<li><a href="#detail-tabs-description"> Details</a></li>
					
					{if $auction.terms}<li><a href="#detail-tabs-terms"> Terms</a></li>{/if}
					{if $auction.document}<li><a href="#detail-tabs-documents"> Documents</a></li>{/if}
					{if $auction.virtual_tour_link or $auction.online_bidding_link or $auction.online_pre_bidding_link or $auction.general_all_purpose_link or $auction.online_presentation or $auction.more_info_link}<li><a href="#detail-tabs-links"> Links</a></li>{/if}
					{if $auction.catalog}<li><a href="#detail-tabs-catalog"> Catalog</a></li>{/if}
				<div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="http://{$smarty.server.HTTP_HOST|strip_tags}{$smarty.server.REQUEST_URI|strip_tags}" addthis:title="{$auction.title|strip_tags}" addthis:description="{$auction.short_description|strip_tags}" style="float:right;margin:-3px 0 0 0">
					<a class="addthis_button_facebook"></a>
					<a class="addthis_button_linkedin"></a>
					<a class="addthis_button_stumbleupon"></a>
					<a class="addthis_button_email"></a>
					<a class="addthis_button_favorites"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
				</div>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=auctionservices"></script>
				</ul>
				<div id="detail-tabs-description">				
					<!-- AUCTION DETAILS -->
					{if $auction.details}
					<div class="tabsection">
						<p>{$auction.details}</p>
					</div>
					{/if}
				<!-- END AUCTION DETAILS -->			
				</div><!-- END DETAIL TAB DESCRIPTION -->			
			{if $auction.terms}
				<div id="detail-tabs-terms">
					<!-- begin terms view -->
					{if $auction.terms}
						<div class="tabsection">
							<p>{$auction.terms}</p>
						</div>
					{/if}
					<!-- end terms view -->
				</div><!-- END DETAIL TAB TERMS -->
			{/if}	
			{if $auction.document}
				<div id="detail-tabs-documents">
					<div class="tabsection">
						<div class="acListEventBiddingLink">
							{foreach from=$auction.document item=document}
								{if $document.is_pip eq 'true'}
									<div id="pip-form-modal-{$document.id}" title="PIP Form">
										<iframe src="http://ignite.auctionservices.com/support/auth_document_form/{$document.id}&width=768" style="width:100%;height:100%;border:0px;" frameborder="0" border="0"></iframe>
									</div>
									<!-- Modal JavaScript Call for pip form viewer -->
									{literal}
										<script type="text/javascript">
											// Initiate modal box
											jQuery(function() {
													jQuery( "#pip-form-modal-{/literal}{$document.id}{literal}" ).dialog({
													height: 768,
													width: 1024,
													modal: true,
													autoOpen: false,
													draggable: false,
													resizable: false,
													buttons: { "Close": function() { jQuery(this).dialog("close"); } }
												});
											});
										</script>
									{/literal}
									<h3><a onclick="jQuery( '#pip-form-modal-{$document.id}' ).dialog('open');">{$document.title} - Click here</a></h3>
								{else}
									<h3><a href="{$document.url}" target="_new">{$document.title} - Click here</a></h3>
								{/if}
							{/foreach}
						</div>
					</div>				
				</div><!-- END DETAIL TAB DOCUMENTS -->
			{/if}		
			{if $auction.virtual_tour_link or $auction.online_bidding_link or $auction.online_pre_bidding_link or $auction.general_all_purpose_link or $auction.online_presentation or $auction.more_info_link}	
				<div id="detail-tabs-links"><!-- EXTERNAL LINKS TAB -->
					<div class="tabsection">
						<div class="acListEventBiddingLink">						
							<!-- virtual tour link -->
							{foreach from=$auction.virtual_tour_link item=link}
								<h3 class="acListEventBiddingLink"><a href="{$link.url}" target="_blank" title="{#virtualTourText#}">{#virtualTourText#} - Click here</a></h3>
							{/foreach}
							<!-- end virtual tour link -->							
							<!-- online bidding link -->
							{foreach from=$auction.online_bidding_link item=link}
								<h3><a href="{$link.url}" target="_blank" title="{#onlineBiddingText#}">{#onlineBiddingText#} - Click here</a></h3>
							{/foreach}
							<!-- end online bidding link -->		
							<!-- online pre-bidding link -->
							{foreach from=$auction.online_pre_bidding_link item=link}
								<h3><a href="{$link.url}" target="_blank" title="{$singular_text}">{#onlinePreBiddingText#} - Click here</a></h3>
							{/foreach}
							<!-- end online pre-bidding link -->        
							<!-- links link -->
							{foreach from=$auction.general_all_purpose_link item=link}
								<h3><a href="{$link.url}" target="_blank" title="{#linkText#}">{$link.title} - Click here</a></h3>
							{/foreach}
							<!-- end links link -->      
							<!-- online presentation link -->
							{foreach from=$auction.online_presentation item=link}
								<h3><a href="{$link.url}" target="_blank" title="{#onlinePresentationText#}">{#onlinePresentationText#} - Click here</a></h3>
							{/foreach}
							<!-- end online presentation link -->        
							<!-- more info link -->
							{foreach from=$auction.more_info_link item=link}
								<h3><a href="{$link.url}" target="_blank" title="{#moreInformationText#}">{#moreInformationText#} - Click here</a></h3>
							{/foreach}
							<!-- end more info link -->							
						</div><!-- END button-links -->
					</div>
				</div><!-- END EXTERNAL LINKS TAB-->
			{/if}  
			{if $auction.catalog}
				<div id="detail-tabs-catalog">
					<div class="acListEventBiddingLink">	
						<a href="http://ignite.auctionservices.com/auctions/{$auction.id}/catalog_list" target="_blank">Click here to view catalog.</a>
						</div>
				</div><!-- END DETAIL TAB CATALOG -->
			{/if}			
		</div><!-- END DETAIL INNER TABS -->
	</div>
      
      {if $auction.video_link}
      <!-- START VIDEOS TAB -->
				<div id="detail-tabs-video">
					<!-- begin video view -->
						{foreach from=$auction.video_link item=video}
							<div class="tabsection">
								<h3 class="video_win">{#videoText#}: {$video.title}</h3>
								<p>{$video.html}</p>
							</div>
						{/foreach}
					<!-- end video view --> 
				</div>
      <!-- END VIDEOS TAB -->
			{/if}	  
  
	  <!-- START DIRECTIONS -->
	  {if $auction.directions or $auction.location}
		<div id="tabs-directionsmap">		
			  {if $auction.directions}
				<div class="tabsection">
					  <p>{$auction.directions}</p>
				</div>
			  {/if}	
			  {if $auction.location}
				<div class="tabsection">
          <div align="center"><iframe width="500" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='{$auction.location.first_address} + {$auction.location.city} + {$auction.location.state.abbreviation} + {$auction.location.zipcode}'&ie=UTF8&hq=&hnear='{$auction.location.first_address} + {$auction.location.city} + {$auction.location.state.abbreviation} + {$auction.location.zipcode}'&gl=us&t=m&z=12&iwloc=A&output=embed"></iframe></div>
				</div>
			  {/if}			
		</div>
	 {/if}
	 <!-- END DIRECTIONS -->	
  	{if ($auction.image and count($auction.image) > 1) or count($auction.gallery) > 0}
		<div id="detail-tabs-morephotos">
		{if ($auction.image and count($auction.image) > 1)}
			<div class="tabsection">
				<p>{#clickAnyImageText#}</p>
					<p><table>
					{counter start=0 print=false assign=TableCell}
					{foreach from=$auction.image item=image}
					{if $TableCell % 5 == 0}
					<tr>
					{/if}
						  <td class="galleryTable">
							 <a class='primary-gallery' href='{$image.large_url}' title="{$image.title} {$image.description}">
								  <img class="lazy" src="{$assets_url}/images/loading.gif" data-original="{$image.tiny_url}" title="{$image.title}" alt="{$image.title}" />
							 </a>
						  </td>
						  {if $TableCell % 5 == 4}
					</tr>
						  {/if}
					{counter}
					{/foreach}
					</table></p>
{literal}<script type="text/javascript">jQuery('a.primary-gallery').colorbox({rel:'primary-gallery', transition:"none"});</script>{/literal}
			</div>
		{/if}				
					<!-- Image Gallery Navigation - Only shows up for multiple galleries -->
		{if count($auction.gallery) gt 1}
			<div id="ignite-image-gallery-navigation" class="tabsection">
				<h3 class="photo_galleries">Photo Galleries</h3>
				<p>Click a gallery name to jump to those photos.</p>
				{foreach from=$auction.gallery item=gallery}
					{if count($gallery.image) > 0} 
						<p><a class="" href="#ignite-image-gallery-{$gallery.id}">{$gallery.title}</a></p>
					{/if}
				{/foreach}					
			</div><!-- END ignite-image-gallery-navigation -->
		{/if}
					<!-- END Image Gallery Navigation -->				
		{foreach from=$auction.gallery item=gallery}
			{if count($gallery.image) > 0}
				<a name="ignite-image-gallery-{$gallery.id}"></a>
					<div class="tabsection">
						<h3 class="photo_gallery">{#galleryText#}: {$gallery.title}</h3>
						<p>{#clickAnyImageText#}</p>
						<p><table>
						{counter start=0 print=false assign=TableCell}
							{foreach from=$gallery.image item=image}
								  {if $TableCell % 5 == 0}
									<tr>
								  {/if}
										  <td class="galleryTable" style="width:110px; height:85px; text-align:center; vertical-align:middle;">
											<a class='gallery-{$gallery.id}' href='{$image.large_url}' title="{$image.title}"><img class="lazy" src="{$assets_url}/images/loading.gif" data-original="{$image.tiny_url}" title="{$image.title}" alt="{$image.title}" /></a>
										  </td>
								  {if $TableCell % 5 == 4}
									</tr>
								  {/if}
						 {counter}
							{/foreach}
								</table></p>
{literal}<script type="text/javascript">jQuery('a.gallery-{/literal}{$gallery.id}{literal}').colorbox({rel:'gallery-{/literal}{$gallery.id}{literal}', transition:"none"});</script>{/literal}
					</div>
			{/if}
		{/foreach}
{literal}<script type="text/javascript">jQuery("img").lazyload({effect:"fadeIn",event:"scrollstop"});</script>{/literal}
			</div><!-- END DETAIL TAB PHOTOS -->
		  	{/if}
	<!-- START CONTACTS -->
	<div id="tabs-contact" align="center">
		{if $auction.company.web_site}
			<div class="contact_tab">		
  			{if $auction.company.name}<p>{$auction.company.name}</p>{/if}
  				<p><a href="{$auction.company.web_site}" target="_blank">{$auction.company.web_site}</a></p>
  		{if $auction.company.location}<p>{$auction.company.location.first_address} {$auction.company.location.city}, {$auction.company.location.state.abbreviation} {$auction.company.location.zipcode}</p>{/if}
  		{if $auction.company.email}<div class="acListEventBiddingLink" style="text-align:center;"><p><a href="mailto:{$auction.company.email}?subject=Contact Submission from Auction: {$auction.title|strip_tags:true}">EMAIL THE AUCTIONEER</a></p></div>{/if}
  			</div> 
	  	{/if}
	</div>
	<!-- END CONTACTS -->
	<!-- START PRINT CONFIRMATION -->
	<div id="tabs-print">
		<h3>Your auction should be printing now. If not, please <a href="javascript://" onclick="return ignite_print('auction', {$auction.id});" title="{#printAuctionText#}">click here</a>.</h3>
	</div>
	<!-- END PRINT CONFIRMATION -->
</div>
<!-- BEGIN AUCTIONSERVICES FOOTER -->
<div id="acDetailFooter"><a href="http://www.auctionservices.com" target="_blank" title="{#auctionServicesText#}"><img src="http://static.auctionservices.com/logos/pbas.gif" alt="{#auctionServicesText#}" /></a></div><div style="height:1px;"><img src="http://ignite.auctionservices.com/auctions/{$auction.id}/hitvisit.gif?{$smarty.now}" alt="" border="0" />
</div>
</div>
<!-- END AUCTIONSERVICES FOOTER -->
<!-- BUTTONIZE OUR BUTTONS -->
<script>{literal}jQuery(function(){jQuery("a",".button-links").button();});{/literal}</script>
<!-- END WORDPRESS AUCTION DETAIL TEMPLATE -->