<?php get_header(); ?>
<section class="main single" itemscope itemtype="http://schema.org/Article">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="post">
			<h1><?php the_title(); ?> <?php edit_post_link(esc_attr__('Edit this entry', 'multipurpose'), '', ''); ?></h1>
			<p class="post-meta"><?php the_time(get_option( 'date_format')) ?><span>|</span> <?php esc_attr__('by', 'multipurpose') ?> <?php the_author(); ?> <span>|</span> <?php the_category(", "); ?> <?php if ( comments_open() ) : ?><?php comments_popup_link('0', '1', '%', 'comment-link'); ?><?php endif; ?></p>
			<?php 
			$iframe_video = get_post_meta(get_the_id(), 'single_meta_video_iframe', true);
			if($iframe_video):
				echo '<p>'.$iframe_video.'</p>'; 
			else :
				if(has_post_thumbnail()) {
					$sidebar_position = get_post_meta($thisPageId, 'sidebar_position', true);
					if ($sidebar_position == 2) {
						the_post_thumbnail('thumbnail-high-extra-large');
					} else {
						the_post_thumbnail('thumbnail-high-large');
					}
				}
			endif; ?>
			<?php the_content(); ?>
			<?php wp_link_pages(array('before' => '<p class="pages"><strong>'.esc_attr__('Pages', 'multipurpose').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			<?php if(has_tag()): ?><p class="tags"><?php esc_attr_e('Tags', 'multipurpose'); ?>: <?php the_tags(""); ?></p><?php endif; ?>
			<?php 
			$share_links = get_theme_mod('share_links');
			if (empty($share_links)) $share_links = 2;
			if($share_links == 2) : 
				get_template_part('share');
			endif; 
			?>
		</article>

		<?php
		$show_author = get_theme_mod('show_post_author');		
		if(!$show_author):
		?>
			<section class="post-author">
				<?php echo get_avatar(get_the_author_meta( 'ID' )) ?>
				<div>
					<h3>Author: <?php the_author_link(); ?></h3>
					<p><?php echo get_the_author_meta( 'description' ); ?></p>
				</div>
			</section>
		<?php endif; ?>

		<?php 
		$show_related = get_theme_mod('show_related');
		if(!$show_related) related_posts($post);
		
		comments_template(); 
		?>

		<nav class="project-nav">
			<span class="prev"><?php next_post_link('%link'); ?></span>
			<span class="next"><?php previous_post_link('%link'); ?></span>
		</nav>


	<?php endwhile; endif; ?>
</section>
<?php 
$sidebar_position = get_post_meta($thisPageId, 'sidebar_position', true);
if($sidebar_position == 3) $sidebar_position = $sidebar_pos_global;
if($sidebar_position != 2) {
	$sidebar = get_post_meta(get_the_id(), 'custom_sidebar', true) ? get_post_meta(get_the_id(), 'custom_sidebar', true) : "default";
	if($sidebar != 'no') {
		if($sidebar && $sidebar != "default") get_sidebar("custom");
		else get_sidebar();	
	}
}
?>
<?php get_footer(); ?>
