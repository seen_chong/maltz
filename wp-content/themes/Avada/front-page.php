<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="jumbotron" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/jumbotron_img.png)">
    <div class="banner_text">
        <h1>$10,400,000 - East Quogue, NY</h1>
        <a href="/upcoming-auctions">
            <h6>Upcoming Auctions</h6>
        </a>
    </div>
</div>

<div class="featured_in_section">
    <div class="standard_contain">


		<div class="featured_in_header">
			<h1><span>As Featured In</span></h1>
		</div>

		<div class="featured_in_list ">
			<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/times_herald_logo.png">
			</div>
						<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/realtor_logo.png">
			</div>
						<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/newsday_logo.png">
			</div>
						<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/daily_logo.png">
			</div>
						<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/tandc_logo.png">
			</div>
		</div>
	</div>
	
</div>


<div class="about_us_section">
    <div class="standard_contain">


		<div class="featured_in_header">
			<h1><span>About Us</span></h1>
		</div>

		<div class="about_us_text">
            <p>Maltz Auctions is a premier, full-service auction company specializing in creating liquidity through the auction method of marketing. Now a $100+ million company with an impeccable reputation for professionalism, integrity and success, Maltz Auctions has conducted thousands of auctions and generated over $2 billion in sales on behalf of such clients as the U.S. Bankruptcy Courts and financial institutions throughout the country, the U.S. Department of Justice, the Securities and Exchange Commission, the Internal Revenue Service, N.Y. State Department of Taxation & Finance, Court Appointed Receivers, Estate Executors and Public Administrators, as well as law firms, private individuals and corporations, nationwide. Maltz Auctions is court approved to act in the capacity of auctioneers, real estate brokers and receivers. The 35-year-old firm is nationally recognized as a trusted name in excellence and achievement.</p>
            
            <div class="holder_btn">
                <a href="/about-us">
                    <button class="btn_classic">Learn More</button>
                </a>
            </div>
		</div>
	</div>
	
</div>


<div class="upcoming_auctions">
		<div class="featured_in_header">
			<h1><span>Featured Auctions</span></h1>
		</div>

		<div class="auction_holdings">

     
                        <?php
            $args = array(
      'post_type' => 'upcoming_auctions',
                'posts_per_page' => 2,
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            <a href="<?php the_permalink(); ?>">
                <div class="auction_holder" id="row_2" style="background-image:url(<?php the_field('featured_image'); ?>);">
                    <div class="overlay">
                        <div class="auction_holder_contain">
                            <div class="auction_detail">
                                <h4><?php the_field('listing_name'); ?></h4>
                                <p><?php the_field('city/state'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
                        <?php
            }
                  }
            else  {
            echo 'No Auctions Found';
            }
      ?> 


        </div>  
</div> 

<div class="upcoming_auctions" id="upcoming_auctions_alt">
		<div class="featured_in_header">
			<h1><span>Upcoming Auctions</span></h1>
		</div>

		<div class="auction_holdings">
		
            <a href="/upcoming-auctions/residential-properties/">
                <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_res.jpg);">
                    <div class="overlay">
                        <div class="auction_holder_contain">
                            <div class="auction_detail">
                                <h5>Residential Properties</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="/upcoming-auctions/commercial-properties/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_comm.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Commercial Properties</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/distinctive-properties/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_dist.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Distinctive Properties</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>  
    
    		<div class="featured_in_header">
			<h3>We offer more than real estate, additional auction divisions</h3>
		</div>

		<div class="auction_holdings">
		  
            <a href="/upcoming-auctions/autos-boats/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_auto.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Autos & Boats</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/jewelry-collectibles/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_jewel.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Jewelry & Collectibles</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/unique-assets/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_unique.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Unique Assets</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/heavy-equipment/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_heavy.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Heavy Equipment</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/luxury-assets/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_boats.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Luxury Assets</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/going-concerns-leaseholds/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_lease.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Going Concerns & Leaseholds</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/business_assets_inventory/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_business.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Business Assets & Inventory</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/">
            <div class="auction_holder" id="extend_2" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_equity.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Equity Advance</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
        </div>  
</div> 
<div class="test_section">
			<div class="featured_in_header">
			<h1><span>Testimonials</span></h1>
		</div>
<div class="wrapper">
<h4>Testimonials from a few of our past sellers, buyers, and fiduciares.</h4>
			
			<div class="slick_slider">

					<?php
			  $args = array(
			    'post_type' => 'testimonials'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>
</div>
</div>


<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>

</div>


<script type="text/javascript">

$(document).ready(function(){
  jQuery('.slick_slider').slick({
  	        dots: false,
        infinite: true
  });
});
    
    $(".auction_holder").hover(function(){
    $(".overlay",this).slideToggle(500);
        
});
		
</script>

<?php get_footer(); ?>